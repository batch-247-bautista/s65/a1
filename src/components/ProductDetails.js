import { useParams, useNavigate, Link } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
const { user } = useContext(UserContext);
const navigate = useNavigate();
const { productId } = useParams();
const [name, setName] = useState("");
const [description, setDescription] = useState("");
const [price, setPrice] = useState(0);
const [userOrders, setUserOrders] = useState(0);
const [availableSize, setAvailableSize] = useState("");

useEffect(() => {
fetch(${process.env.REACT_APP_API_URL}/products/${productId}/details)
    .then(res => res.json())
    .then(data => {
    setName(data.name);
    setDescription(data.description);
    setPrice(data.price);
    setUserOrders(data.userOrders.length);
    setAvailableSize(data.availableSize);
    })
    .catch(err => console.error(err));
    }, [productId]);

    const editProduct = () => {
    navigate(/products/${productId}/edit);
    };

    return (
    <Container>
        <Row>
            <Col lg={{ span: 6, offset: 3 }}>
                <Card>
                    <Card.Body className="text-center">
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                    <Card.Subtitle>Available Size:</Card.Subtitle>
                    <Card.Text>{availableSize}</Card.Text>
                    <Button className="btn btn-info mr-2" as={Link} to="/sizes">1 Size</Button>
                    {user.id !== null ? (
                    <>
                    <Card.Subtitle className="mb-2">Current Number of UserOrders:</Card.Subtitle>
                    <Card.Text className="mb-3">{userOrders}</Card.Text>
                    {user.isAdmin ? (
                    <div className="d-flex justify-content-between align-items-center">
                    <Button variant="primary" onClick={() => editProduct()} style={{ width: "250px" }}>Edit</Button>
                    <div style={{ width: "20px" }}></div>
                    <Button variant="secondary" as={Link} to="/products" style={{ width: "250px" }}>Back</Button>
                    </div>
                    ) : (
                    <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>)}
                    </>
                    ) : (
                    <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
                    )}
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
);
}