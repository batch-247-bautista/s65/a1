import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';

import { useState, useEffect } from 'react';

export default function AllProducts() {
	
	
	const [ allProducts, setAllProducts ] = useState([]);
	const [ isLoading, setIsLoading] = useState(true);


	// Retrieves the products from the database upon initial render of the "Products" component.
	useEffect(() => {
		fetch(`https://capstone2-ecom-bautista.onrender.com/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "products" state to map the data retrived from the fetch request into several "ProductCard" components
			setAllProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))

			setIsLoading(false);
		})
	}, [])

	return(
		(isLoading) ?
			<Loading />
		:
		<>
			{products}
		</>
	)

}